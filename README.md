# iocshutils conda recipe

Home: "https://github.com/paulscherrerinstitute/iocsh_utilities"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS iocshutils module
